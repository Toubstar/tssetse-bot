import os
import discord
import random
import picamera
from discord.ext import commands
from dotenv import load_dotenv

def setup(bot: commands.Bot):
        bot.add_cog(Printer(bot))

class Printer(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self._members = {}

    @commands.command(name='printer', help='Gets printer status')
    async def _printer(self, ctx, *, member: discord.Member = None):
        member = member or ctx.author

        msg = f'Tobi\'s first bot command'

        embed = discord.Embed(description=f'**About Partybot** \n\n {msg}')
        await ctx.send(embed=embed)

    @commands.command(name='printerpic', help='Gets printer status picture')
    async def _printer(self, ctx, *, member: discord.Member = None):
        member = member or ctx.author

        with picamera.PiCamera() as camera:
            camera.resolution = (640,480)
            camera.capture("printer_pic.jpg")

        pic=discord.File("printer_pic.jpg")
        await ctx.send("Printer pic", file=pic)
       
